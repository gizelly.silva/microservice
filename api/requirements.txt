coverage==4.5.3
Django==2.2.1
psycopg2==2.8.2
pytz==2019.1
sqlparse==0.3.0
whitenoise==4.1.2
